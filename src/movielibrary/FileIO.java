/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movielibrary;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lehti
 */
public class FileIO {
    private final String filename = "moviedata.dat";
    List<Movie> movieList = new ArrayList<>();
    
    public FileIO(){
        System.out.println("movielibrary.FileIO.<init>()");
    }
    
    public boolean checkForMissingFile(){
        System.out.println("movielibrary.FileIO.checkForMissingFile()");
        //check to see if file exists, if not init dev data
        File f = new File(filename);
        //if file doesnt exist, make a file
        if(!f.exists() && !f.isDirectory()) { 
        //if(!f.isFile()){
            try {
                f.createNewFile();
                return true;
            } catch (IOException ex) {
                Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
    public boolean writeObject(String file, Movie movie) throws FileNotFoundException, IOException {
        if(file == null){
                return false;
        }
        if(file.isEmpty()){
                return false;
        }
        
        try{
            FileOutputStream fos;
            if(!file.isEmpty()){
               fos = new FileOutputStream(file, true);
            }
            else{
                fos = new FileOutputStream(file);
            }
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(movie);
            oos.close();
            fos.close();
        }
        catch(FileNotFoundException ex){
            return false;
        }
        catch(IOException ex){
            return false;
        }
        return true;
    }
    
    public Movie readObject(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Movie movie = new Movie();
        try{
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream( fis );
            movie = (Movie) ois.readObject();
            ois.close();
            fis.close();
        }
        catch(FileNotFoundException ex){
            return null;
        }
        return movie;
    }
    
    private boolean writeObjectList(List<Movie> movieList){
        System.out.println("movielibrary.FileIO.writeObjectList()");
        try{
            FileOutputStream fos;
            ObjectOutputStream oos;
            
            //always rewrite the file
            fos = new FileOutputStream(filename);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(movieList);
            oos.close();
            fos.close();
        }
        catch(FileNotFoundException ex){
            return false;
        }
        catch(IOException ex){
            return false;
        }
        return true;
    }
    
    public List<Movie> readObjectList(){
        System.out.println("movielibrary.FileIO.readObjectList()");
        List<Movie> loadedMovieList = new ArrayList<>();
        FileInputStream fis;
        ObjectInputStream ois;
        try{
            fis = new FileInputStream(filename);
            //check fis has something in it, 0 is eof
            if(fis.available() > 0){
                ois = new ObjectInputStream( fis );

                loadedMovieList = (List <Movie>)ois.readObject();
                
                ois.close();
            }
            fis.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return loadedMovieList;
    }
    
    public boolean addNewMovie(Movie movie){
        System.out.println("movielibrary.FileIO.addNewMovie()");
        //new and old list, append old stuff to new and add current
        List<Movie> newMovieList = new ArrayList<>();
        List<Movie> oldMovieList = new ArrayList<>();
        
        oldMovieList = (List<Movie>)readObjectList();
        //populate new list with the old data and add latest
        newMovieList.addAll(oldMovieList);
        newMovieList.add(movie);
        writeObjectList(newMovieList);
        return true;
    }
    
    public void initDevData() throws IOException{
        System.out.println("---Creating devdata in " + filename);
        
        Movie movie1 = new Movie();
        Movie movie2 = new Movie();
        Movie movie3 = new Movie();
        
        movie1.setMovieId(0);
        movie1.setMovieGenre("Drama , Horror , Mystery");
        movie1.setMovieName("They Look Like People");
        movie1.setMovieComment("mind twister");
        movie1.setMovieReleaseYear(2015);
        movie1.setMovieRating("Not rated");
        movie1.setMovieStarRating(9);
        movie1.setStarred(true);
        movie1.setWatched(true);
        
        movie2.setMovieId(1);
        movie2.setMovieGenre("Biography , Drama , Thriller");
        movie2.setMovieName("Captain Phillips");
        movie2.setMovieComment("Tom Hanks");
        movie2.setMovieReleaseYear(2013);
        movie2.setMovieRating("K-16");
        movie2.setMovieStarRating(7);
        movie2.setStarred(false);
        movie2.setWatched(true);
        
        movie3.setMovieId(2);
        movie3.setMovieGenre("Action , Adventure , Sci-Fi");
        movie3.setMovieName("Aliens");
        movie3.setMovieComment("");
        movie3.setMovieReleaseYear(1986);
        movie3.setMovieRating("K-16");
        movie3.setMovieStarRating(8);
        movie3.setStarred(true);
        movie3.setWatched(true);
        
        addNewMovie(movie1);
        addNewMovie(movie2);
        addNewMovie(movie3);
    }
}
