/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movielibrary;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lehti
 */
public class Controller {
    private MovieView view;
    private Movie movie;
    
    Controller(Movie movie, MovieView view) {
        System.out.println("movielibrary.Controller.<init>()");
        this.view = view;
        this.movie = movie;
        view.setVisible(true);
        boolean DEBUGMESSAGES = true;
    }
    
    /**
     *
     * @throws java.io.IOException
     * @throws java.io.FileNotFoundException
     * @throws java.lang.ClassNotFoundException
     */
    
    public void printSystemOut(){
        System.out.println(movie.getMovieId());
        System.out.println(movie.getMovieGenre());
        System.out.println(movie.getMovieName());
        System.out.println(movie.getMovieReleaseYear());
        System.out.println(movie.getMovieStarRating());
        System.out.println(movie.isStarred());
        System.out.println(movie.isWatched());
    }
    
    public void doStuffs(){
        view.populateComboBox();
    }
}
