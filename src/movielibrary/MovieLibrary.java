/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movielibrary;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author lehti
 * movie library with rating system, comment section, related movies
 * genre,
 */
public class MovieLibrary {
    
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     * @throws java.lang.ClassNotFoundException
     */
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException{
        FileIO fileio = new FileIO();
        if(fileio.checkForMissingFile()){
            fileio.initDevData();
        }
        
        MovieView view = new MovieView();
        view.setVisible(true);
    }
}
