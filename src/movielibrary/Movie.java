/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movielibrary;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author lehti
 */
public class Movie implements Serializable{
    private final int RELEASEYEAR_MIN = 1899;
    
    private int movieId;
    private int movieReleaseYear;
    private String movieName;
    private String movieGenre;
    private int movieStarRating;
    private String movieRating;
    private String movieComment;
    private boolean watched;
    private boolean watchList;
    private boolean starred;
    
    // <editor-fold defaultstate="collapsed" desc="Get/Set">    
    public int getMovieId() {
        return movieId;
    }

    public boolean setMovieId(int movieId) {
        if (movieId > 0){
            this.movieId = movieId;
            return true;
        }
        else{
            return false;
        }
    }

    public int getMovieReleaseYear() {
        return movieReleaseYear;
    }

    public void setMovieReleaseYear(int movieReleaseYear) {
        this.movieReleaseYear = movieReleaseYear;
    }
    public boolean setMovieReleaseYear2(int movieReleaseYear) {
        if(movieReleaseYear > RELEASEYEAR_MIN){
            this.movieReleaseYear = movieReleaseYear;
            return true;
        }
        else{
            return false;
        }
    }

    public String getMovieName() {
        return movieName;
    }

    
    public void setMovieName(String movieName) {
        String trimmedMovieName = movieName.trim();
        if(!trimmedMovieName.isEmpty()){
            this.movieName = trimmedMovieName;
        }
    }
    //check if empty, trim leading and trailing spaces, make sure what remains is not empty
    public boolean setMovieName2(String movieName) {
        if( movieName == null){
            return false;
        }
        else{
            String trimmedMovieName = movieName.trim();
            if(!trimmedMovieName.isEmpty()){
                this.movieName = trimmedMovieName;
                return true;
            }
            else{
                return false;
            }
        }
    }

    public String getMovieComment() {
        return movieComment;
    }

    public void setMovieComment(String movieComment) {
        String trimmedMovieComment = movieComment.trim();
        this.movieComment = trimmedMovieComment;
    }
    public boolean setMovieComment2(String movieComment) {
        String trimmedMovieComment = movieComment.trim();
        if(!isWatched() && !trimmedMovieComment.isEmpty()){
            return false;
        }
        else{
            this.movieComment = trimmedMovieComment;
            return true;
        }
    }
    
    public String getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }

    public int getMovieStarRating() {
        return movieStarRating;
    }

    public void setMovieStarRating(int movieStarRating) {
        this.movieStarRating = movieStarRating;
    }

    public String getMovieRating() {
        return movieRating;
    }

    public void setMovieRating(String movieRating) {
        this.movieRating = movieRating;
    }

    public boolean isWatched() {
        return watched;
    }

    public void setWatched(boolean watched) {
        this.watched = watched;
    }

    public boolean isWatchList() {
        return watchList;
    }

    public void setWatchList(boolean watchList) {
        this.watchList = watchList;
    }

    public boolean isStarred() {
        return starred;
    }

    public void setStarred(boolean starred) {
        this.starred = starred;
    }
    // </editor-fold>
}
